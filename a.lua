-- https://gitlab.com/kuklochai/tempcc/-/raw/master/a.lua

local pretty = require "cc.pretty"
pretty.write(pretty.dump({1, 2, 3}))
pretty.write(pretty.group(pretty.text("hello") .. pretty.space_line .. pretty.text("world")))